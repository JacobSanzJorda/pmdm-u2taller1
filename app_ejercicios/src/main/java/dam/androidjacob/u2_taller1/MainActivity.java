package dam.androidjacob.u2_taller1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;
    private TextView tvDisplay;
    private Button buttonIncrease, buttonIncreaseTwo, buttonDecrease, buttonDecreaseTwo, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        setUI();
    }


    private void setUI(){
        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonIncreaseTwo = findViewById(R.id.buttonIncreaseTwo);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonDecreaseTwo = findViewById(R.id.buttonDecreaseTwo);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonIncreaseTwo.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonDecreaseTwo.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonIncrease: count++; break;
            case R.id.buttonIncreaseTwo: count += 2; break;
            case R.id.buttonDecrease: count--; break;
            case R.id.buttonDecreaseTwo: count -= 2; break;
            case R.id.buttonReset: count = 0; break;
        }

        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("tvDisplay");
        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tvDisplay",count);
    }
}